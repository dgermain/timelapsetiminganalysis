import argparse
import os
import time

# RAC - Recherche d'anomalie chronologique

#this call Exiftools on the command line to extract creation time for files.
def callExif(textFile, inputFolder, fileExtension):
    myCmd = 'exiftool -CreateDate ' + '"' + inputFolder + '"*.' + fileExtension + ' > ' + '"' +textFile+'"'
    os.system(myCmd)
    pass


#Read saved textfile and extract createDate for each file
def readTextFile(textFile):
    fileTime = {}

    file = open(textFile, 'r')
    lines = file.readlines()

    imageFileName = ""
    for li in lines:
        li = li.strip()
        if li.startswith("========"):
            imageFileName = li[len("======== "):]

            #Find last folder / and only take filename.
            pos = imageFileName.rfind('/')
            imageFileName = imageFileName[pos+1:]
        elif li.startswith("Create Date"):
            pos = li.find(":")
            timestamp = li[pos+2:]
            parsedTime = time.strptime(timestamp, "%Y:%m:%d %H:%M:%S")
            fileTime[imageFileName] = parsedTime

    return fileTime



def processDeltaTiming(textFile, nametag, outputFolder):
    fileTimeDict = readTextFile(textFile)

    filenameList = []
    fileToCreationTime = {}
    filenameToDelta = {}

    #Create Dict to acces create time per filename
    #Create list of all filenames for ordering
    for filename, fileTime in fileTimeDict.items():
        filenameList.append(filename)
        fileToCreationTime[filename] = fileTime

    filenameList.sort()

    #Group files in list using their deltaTime as key for the dictionnary
    #Those list are in the correct order because filled from a sorted list.
    deltaTimeDict = {}

    #Calculate the delta time for all file except first one.
    previousFileTime = None
    for currentFilename in filenameList:
        currentFileTime = fileToCreationTime[currentFilename]

        if previousFileTime is not None:

            delta =(time.mktime(currentFileTime) - time.mktime(previousFileTime))

            filenameToDelta[currentFilename] = delta

            #Add creationTime in group of deltatime
            if delta not in deltaTimeDict:
                deltaTimeDict[delta] = []
            deltaTimeDict[delta].append(currentFilename)

        previousFileTime = currentFileTime

    #List all the different deltatime we have found
    allDeltas = []
    for k in deltaTimeDict.keys():
        allDeltas.append(k)

    #Sorted them from biggest to lowest
    allDeltas.sort(reverse=True)

    # Save the deltatime to a file
    deltaTimeFile = outputFolder + nametag + '_largestFirstDeltaTimes.txt'
    outFile = open(deltaTimeFile, "w+")

    for d in allDeltas:
        for currentFilename in deltaTimeDict[d]:
            outFile.write(currentFilename + "," + str(d) + "\n")
            print(currentFilename + " " + str(d) + "s")

    outFile.close()

    # Save the deltatime sequentially to a file
    sequentialDeltaTimeFile = outputFolder + nametag + '_sequentialDeltaTimes.txt'
    seqOutFile = open(sequentialDeltaTimeFile, "w+")

    for currentFilename in filenameList:
        if currentFilename in filenameToDelta.keys():
            seqOutFile.write(currentFilename + "," + str(filenameToDelta.get(currentFilename)) + "\n")

    seqOutFile.close()

    #Return how many delta founds
    return len(fileToCreationTime)

def main():

    # Training settings
    parser = argparse.ArgumentParser(description='Analysis consistency of timelapse photo taking')

    parser.add_argument("-o", "--output", default='./analysis/', help='Path to save the data Files')
    parser.add_argument("-n", "--nametag", default='result', help='prefix Name of output files')

    parser.add_argument("-v", '--verbose', dest='verbose', action='store_true')

    parser.add_argument("-i", '--input', default='./data/', help='Folder containing images')
    parser.add_argument("-x", '--text', default='', help='File containing exiftools creation date')
    parser.add_argument("-t", '--typefile', default='JPG', help='Extension of file in folder to process')

    args = parser.parse_args()

    outputFolder = args.output
    inputFolder = args.input

    textFile = args.text
    nametag = args.nametag

    fileExtension = args.typefile

    verbose = args.verbose

    if not outputFolder.endswith("/"):
        outputFolder = outputFolder + "/"

    if not os.path.isdir(outputFolder):
        print("Cannot find output folder: '" + outputFolder + "'")
        return

    if len(textFile) == 0:
        #Process input folder, and create text file
        if not inputFolder.endswith("/"):
            inputFolder = inputFolder + "/"

        if not os.path.isdir(inputFolder):
            print("Cannot find input folder: '" + inputFolder + "'" )
            return

        #create the text file
        textFile = outputFolder + nametag + '_exifOutput.txt'
        callExif(textFile, inputFolder, fileExtension)

        if not os.path.isfile(textFile):
            print("Text file not properly created: '" + textFile + "'")
            return
    else:
        #Text file was provided but not found
        if not os.path.isfile(textFile):
            print("Provided Text file cannot be found: '" + textFile + "'")
            return

    print('Output Folder: ' + outputFolder)
    print('Input Folder: ' + inputFolder)
    print('Text File: ' + textFile)
    print('Extension of file to process: ' + fileExtension)


    nmbFound = processDeltaTiming(textFile, nametag, outputFolder)

    print("\n------ Found: " + str(nmbFound) + " Elements ----------")


if __name__ == '__main__':
    main()